(function ( ) {

	var calendarClass = 'calendar';

	var calendarInstances = [];

	/* Called when click day of month
	 * @param {Event} e - Event data object
	 */
	function selectDate ( e ) {
		var self = e.target.dataCalendarObject;
		self.setDate ( e.target.dataDay, e.target.dataMonth, e.target.dataYear );
	}

	/* Called when click month
	 * @param {Event} e - Event data object
	 */
	function setMonth ( e ) {
		var self = e.target.dataCalendarObject;
		// self.elements.monthlistcontainer.style.display = 'none';
		self.hideMonthList();
		self.setDate ( null, e.target.dataMonthNumber, null );
	}

	/* Called when double click on month name.
	 * @param {Event} e - Event data object
	 */
	function toggleMonthList ( e ) {
		e.preventDefault();
		var self = e.target.dataCalendarObject;
		if ( self.elements.monthlistcontainer.style.display == 'none' )
			self.showMonthList();
		else
			self.hideMonthList();
	}

	function showYearTextBox ( e ) {
		e.preventDefault();
		var self = e.target.dataCalendarObject;
		self.showYearTextBox();
	}

	function keyonYearTextBox ( e ) {
		var self = e.target.dataCalendarObject;
		if ( e.keyCode == 13 ) {
			self.props.date.setFullYear(self.elements.yeartextbox.value);
			self.redraw();
			self.fireEvent ( 'change', {
				"target" : self
			} );
			self.hideYearTextBox();
		} else if ( e.keyCode == 27 ) {
			self.hideYearTextBox();
		}
	}

	// Called when click on next month button.
	function nextMonth ( e ) {
		var self = e.target.dataCalendarObject;
		var m = self.props.date.getMonth() + 1;
		var y = self.props.date.getFullYear();
		if ( m == 12 ) {
			y ++; m = 0;
			self.props.date.setFullYear(y);
		}
		self.props.date.setMonth(m);
		self.redraw();
		self.fireEvent ( 'change', {
			"target" : self
		} );
	}

	// Called when click on previous month button.
	function previousMonth ( e ) {
		var self = e.target.dataCalendarObject;
		var m = self.props.date.getMonth() - 1;
		var y = self.props.date.getFullYear();
		if ( m == -1 ) {
			y --; m = 11;
			self.props.date.setFullYear(y);
		}
		self.props.date.setMonth(m);
		self.redraw();
		self.fireEvent ( 'change', {
			"target" : self
		} );
	}

	// Called when click on next year button.
	function nextYear ( e ) {
		var self = e.target.dataCalendarObject;
		self.props.date.setFullYear( self.props.date.getFullYear() + 1 );
		self.redraw();
		self.fireEvent ( 'change', {
			"target" : self
		} );
	}

	// Called when click on previous year button.
	function previousYear ( e ) {
		var self = e.target.dataCalendarObject;
		self.props.date.setFullYear( self.props.date.getFullYear() - 1 );
		self.redraw();
		self.fireEvent ( 'change', {
			"target" : self
		} );
	}

	// Calendar object constructor
	var Calendar = function ( el ) {
		calendarInstances.push ( this );
		Calendar.instances.push(this);
		// Object.getOwnPropertyDescriptor(this, "Calendar");
		this.type = 'calendar';

		this.props = {};

		this.props.now = new Date();
		this.props.date = new Date();

		this.props.height = 250; // 250px height
		this.props.width = 266;	 // 266px width, 38 width per cell

		this.props.classnames = {
			"yearpreviousbtn" : "yearpreviousbtn",
			"yearcaption" : "yearcaption",
			"yeartextbox" : "yeartextbox",
			"yearnextbtn" : "yearnextbtn",
			"monthname" : "monthname",
			"monthpreviousbtn" : "monthpreviousbtn",
			"monthnextbtn" : "monthnextbtn",
			"monthlist" : "monthlist",
			"monthlistitem" : "monthlistitem",
			"weekdays" : "weekdays",
			"weekday" : "weekday",
			"monthdays" : "monthdays",
			"monthday" : "monthday",
			"today" : "today",
			"selected" : "selected",
			"previousmonthday" : "previousmonthday",
			"nextmonthday" : "nextmonthday",
			"WeekdayNamesAndMonthdaysContainer" : "wdnamdc",
			"WeekdaynumbersContainer" : "wdnumbers",
			"weeknumber" : "weeknumber"
		};

		this.props.language = 'english';
		this.props.firstday = "sun";
		this.props.daylength = 3;

		this.props.showWeeknumbers = true;
		this.props.wdordsunfirst = ["sun","mon","tue","wed","thu","fri","sat"];
		this.props.wdordmonfirst = ["mon","tue","wed","thu","fri","sat","sun"];
		this.props.wdordtuefirst = ["tue","wed","thu","fri","sat","sun","mon"];
		this.props.wdordwedfirst = ["wed","thu","fri","sat","sun","mon","tue"];
		this.props.wdordthufirst = ["thu","fri","sat","sun","mon","tue","wed"];
		this.props.wdordfrifirst = ["fri","sat","sun","mon","tue","wed","thu"];
		this.props.wdordsatfirst = ["sat","sun","mon","tue","wed","thu","fri"];

		this.props.weekdays = {};

		this.props.weekdays.english = Calendar.locales.english;

		this.props.weekdays.greek = Calendar.locales.greek;

		this.onchange = [];
		this.onselect = [];

		this.addEvent = function ( e, f ) {
			if ( typeof this['on'+e] == 'undefined' || typeof f != 'function' ) return false;
			for ( var i = 0; i < this['on'+e].length; i++ ) if ( this['on'+e][i] === f ) return this;
			this['on'+e].push(f);
			return this;
		};

		this.removeEvent = function ( e, f ) {
			if ( typeof this['on'+e] == 'undefined' ) return false;
			if ( typeof f == 'undefined' ) { this['on'+e] = []; return this; }
			for ( var i = 0; i < this['on'+e].length; i++ ) if ( this['on'+e][i] === f ) {
				this['on'+e].splice(i,1);
				return this;
			}
			return this;
		};

		this.fireEvent = function ( e, obj ) {
			obj.type = e;
			for ( var i = 0; i < this['on'+e].length; i++ ) this['on'+e][i].call(this, obj);
			return this;
		};

		this.addLocale = function ( name, obj ) {
			this.props.weekdays[name] = obj;
			return this;
		};

		this.setLocale = function ( name ) {
			if ( typeof this.props.weekdays[name] == 'undefined' ) {
				console.error ( "The is no such language installed as \"" + name + "\".");
				return false;
			}
			this.props.language = name;
			return this;
		};

		this.setDate = function ( d, m, y) {
			if ( d !== null ) this.props.date.setDate(d);
			if ( m !== null ) this.props.date.setMonth(m);
			if ( y !== null ) this.props.date.setFullYear(y);
			this.redraw();
			this.fireEvent ( 'change', {
				"target" : this
			} );
			return this;
		};

		this.showMonthList = function () {
			this.elements.monthlistcontainer.style.display = '';
			this.elements.weekdaycontainer.style.display = 'none';
			this.elements.monthdayscontainer.style.display = 'none';
			return this;
		};

		this.hideMonthList = function () {
			this.elements.monthlistcontainer.style.display = 'none';
			this.elements.weekdaycontainer.style.display = '';
			this.elements.monthdayscontainer.style.display = '';
			return this;
		};

		this.showYearTextBox = function () {
			this.elements.yeartextbox.style.height = this.elements.yearcaption.offsetHeight + "px";
			this.elements.yeartextbox.style.width = this.elements.yearcaption.offsetWidth + "px";
			this.elements.yeartextbox.style.padding = '0';
			this.elements.yeartextbox.style.margin = '0';

			this.elements.yeartextbox.value = this.props.date.getFullYear();

			this.elements.yearcaption.style.display = "none";
			this.elements.yeartextbox.style.display = "";
			this.elements.yeartextbox.focus();
		};

		this.hideYearTextBox = function () {
			this.elements.yeartextbox.style.display = "none";
			this.elements.yearcaption.style.display = "";
		};

		this.redraw = function () {

			this.elements.container.style.width = this.props.width + 'px';
			this.elements.container.style.height = this.props.height + 'px';

			var cellwidth = this.props.width;
			if ( this.props.showWeeknumbers == true ) {
				this.elements.WeekdaynumbersContainer.style.display = '';
				cellwidth = cellwidth / 8;
			} else {
				this.elements.WeekdaynumbersContainer.style.display = 'none';
				cellwidth = cellwidth / 7;
			}

			var rowheight = this.props.height / 9;

			for ( var i = 0; i < this.elements.monthlistitems.length; i++ ) {
				this.elements.monthlistitems[i].textContent = this.props.weekdays[this.props.language].monthnames[i];
			}

			this.elements.yearcaption.textContent = this.props.date.getFullYear();
			this.elements.monthname.textContent = this.props.weekdays[this.props.language].monthnames[this.props.date.getMonth()];

			for ( i = 0 ; i < 7; i ++ ) {
				this.elements.weekdays[i].textContent = this.props.weekdays[this.props.language].daynames[this.props["wdord"+this.props.firstday+"first"][i]].substr(0,this.props.daylength);
				this.elements.weekdays[i].style.width = cellwidth + 'px';
				this.elements.weekdays[i].style.height = rowheight + 'px';
			}

			var fm = new Date( this.props.date.getFullYear(), this.props.date.getMonth(), 1); // First day of the month
			var fmlen = new Date( this.props.date.getFullYear(), this.props.date.getMonth()+1, 0).getDate();
			var fmdayofweek = fm.getDay(); // First day of the month (weekday)
			var pm = new Date(this.props.date.getFullYear(), this.props.date.getMonth(), 0); // Last day of previous month
			var pmlen = pm.getDate(); // Length of previous month
			var j = pmlen - fmdayofweek + (this.props.firstday=='mon'?2:1) ; // Count begin number
			var m = pm.getMonth(); // Previous month number
			var y = pm.getFullYear(); // Previous month year number

			var ys = new Date( this.props.date.getFullYear(), 0, 1); // First day of the year
			var wn = Math.ceil(( ( ( fm - ys) / 86400000) + 1) /7) - 1;
			this.elements.weeknumber[0].style.width  = cellwidth + 'px';
			this.elements.weeknumber[0].style.height  = rowheight + 'px';
			this.elements.weeknumber[0].style.lineHeight  = rowheight + 'px';
			this.elements.weeknumber[0].innerText = '';
			for ( i = 1; i < 7; i ++) {
				this.elements.weeknumber[i].style.width  = cellwidth + 'px';
				this.elements.weeknumber[i].style.height  = rowheight + 'px';
				this.elements.weeknumber[i].style.lineHeight  = rowheight + 'px';
				this.elements.weeknumber[i].innerText = wn + i;
			}

			if ( j > pmlen ) j -= 7;

			var a = 0;
			var cname = this.props.classnames.previousmonthday;

			for ( var i = 0; i < this.elements.monthdaycells.length; i ++ ) {
				switch ( a ) {
				case 0:
					if ( j > pmlen ) { j = 1; if (m < 11) m++; else { m = 0; y++; } a++; cname = ""; }
					break;

				case 1:
					if ( j > fmlen ) { j = 1; if (m < 11) m++; else { m = 0; y++; } a++; cname = this.props.classnames.nextmonthday; }
					break;
				}

				this.elements.monthdaycells[i].className = cname;
				if ( a == 1 && j == this.props.date.getDate() ) this.elements.monthdaycells[i].className += ' selected';
				if ( this.props.now.getDate() == j && this.props.now.getMonth() == m && this.props.now.getFullYear() == y) this.elements.monthdaycells[i].className += ' today';
				
				this.elements.monthdaycells[i].dataDay = j;
				this.elements.monthdaycells[i].setAttribute("data-day", j);
				this.elements.monthdaycells[i].dataMonth = m;
				this.elements.monthdaycells[i].setAttribute("data-month", m);
				this.elements.monthdaycells[i].dataYear = y;
				this.elements.monthdaycells[i].setAttribute("data-year", y);
				this.elements.monthdaycells[i].textContent = j;
				this.elements.monthdaycells[i].style.width = cellwidth + 'px';
				this.elements.monthdaycells[i].style.height = rowheight + 'px';
				this.elements.monthdaycells[i].style.lineHeight = rowheight + 'px';
				j++;
			}

			return this;
		}

		this.valueOf = function () {
			return this.props.date;
		};

		this.elements = {};

		// this.elements.container = ( typeof el == 'object' && el instanceof HTMLElement ) ? el : document.createElement('div');
		this.elements.container = document.createElement('div');
		this.elements.container.dataCalendarObject = this;
		this.elements.container.style.overflow = 'hidden';

		this.elements.yearcontainer = document.createElement('div');
		this.elements.yearcontainer.style.overflow = "hidden";
		this.elements.container.appendChild( this.elements.yearcontainer );

		this.elements.yearpreviousbtn = document.createElement('div');
		this.elements.yearpreviousbtn.className = this.props.classnames.yearpreviousbtn;
		this.elements.yearpreviousbtn.dataCalendarObject = this;
		this.elements.yearpreviousbtn.addEventListener('click', previousYear);
		this.elements.yearcontainer.appendChild(this.elements.yearpreviousbtn);

		this.elements.yearcaption = document.createElement('div');
		this.elements.yearcaption.className = this.props.classnames.yearcaption;
		this.elements.yearcaption.dataCalendarObject = this;
		this.elements.yearcaption.addEventListener('dblclick', showYearTextBox);
		this.elements.yearcontainer.appendChild(this.elements.yearcaption);

		this.elements.yeartextbox = document.createElement('input');
		this.elements.yeartextbox.type="number";
		this.elements.yeartextbox.className = this.props.classnames.yeartextbox;
		this.elements.yeartextbox.style.display = "none";
		this.elements.yeartextbox.dataCalendarObject = this;
		this.elements.yeartextbox.addEventListener('keyup', keyonYearTextBox);
		this.elements.yearcontainer.appendChild(this.elements.yeartextbox);

		this.elements.yearnextbtn = document.createElement('div');
		this.elements.yearnextbtn.className = this.props.classnames.yearnextbtn;
		this.elements.yearnextbtn.dataCalendarObject = this;
		this.elements.yearnextbtn.addEventListener('click', nextYear);
		this.elements.yearcontainer.appendChild(this.elements.yearnextbtn);

		this.elements.monthcontainer = document.createElement('div');
		this.elements.monthcontainer.style.overflow = "hidden";
		this.elements.container.appendChild( this.elements.monthcontainer );

		this.elements.monthpreviousbtn = document.createElement('div');
		this.elements.monthpreviousbtn.className = this.props.classnames.monthpreviousbtn;
		this.elements.monthpreviousbtn.dataCalendarObject = this;
		this.elements.monthpreviousbtn.addEventListener('click', previousMonth);
		this.elements.monthcontainer.appendChild( this.elements.monthpreviousbtn );

		this.elements.monthname = document.createElement('div');
		this.elements.monthname.className = this.props.classnames.monthname;
		this.elements.monthname.dataCalendarObject = this;
		this.elements.monthname.addEventListener('dblclick', toggleMonthList);
		this.elements.monthcontainer.appendChild( this.elements.monthname );

		this.elements.monthnextbtn = document.createElement('div');
		this.elements.monthnextbtn.className = this.props.classnames.monthnextbtn;
		this.elements.monthnextbtn.dataCalendarObject = this;
		this.elements.monthnextbtn.addEventListener('click', nextMonth);
		this.elements.monthcontainer.appendChild( this.elements.monthnextbtn );

		this.elements.centercontainer = document.createElement('div');
		this.elements.container.appendChild( this.elements.centercontainer );

		this.elements.monthlistcontainer = document.createElement('ul');
		this.elements.monthlistcontainer.style.display = 'none';
		this.elements.monthlistcontainer.className = this.props.classnames.monthlist;
		this.elements.centercontainer.appendChild( this.elements.monthlistcontainer );

		this.elements.monthlistitems = [];
		for ( var i = 0; i < 12; i ++ ) {
			var tmpitm = document.createElement('li');
			tmpitm.dataCalendarObject = this;
			tmpitm.dataMonthNumber = i;
			tmpitm.setAttribute("data-month", i);
			tmpitm.className = this.props.classnames.monthlistitem;

			tmpitm.addEventListener('click', setMonth );
			this.elements.monthlistcontainer.appendChild(tmpitm);
			this.elements.monthlistitems.push(tmpitm);
		}

		this.elements.WeekdaynumbersContainer = document.createElement('div');
		this.elements.WeekdaynumbersContainer.style.overflow = "hidden";
		this.elements.WeekdaynumbersContainer.style.styleFloat = "left";
		this.elements.WeekdaynumbersContainer.style.cssFloat = "left";
		this.elements.WeekdaynumbersContainer.style['float'] = "left";

		this.elements.WeekdaynumbersContainer.className = this.props.classnames.WeekdaynumbersContainer;
		this.elements.centercontainer.appendChild( this.elements.WeekdaynumbersContainer );

		this.elements.weeknumber = [];
		for ( i = 0; i < 7; i ++) {
			var tmpwn = document.createElement('div');
			tmpwn.className = this.props.classnames.weeknumber;
			tmpwn.style.overflow = 'hidden';
			this.elements.WeekdaynumbersContainer.appendChild( tmpwn );
			this.elements.weeknumber.push(tmpwn);
		}

		this.elements.WeekdayNamesAndMonthdaysContainer = document.createElement('div');
		this.elements.WeekdayNamesAndMonthdaysContainer.style.overflow = "hidden";
		this.elements.WeekdayNamesAndMonthdaysContainer.className = this.props.classnames.WeekdayNamesAndMonthdaysContainer;
		this.elements.centercontainer.appendChild(this.elements.WeekdayNamesAndMonthdaysContainer);

		this.elements.weekdaycontainer = document.createElement('div');
		this.elements.weekdaycontainer.style.overflow = "hidden";
		this.elements.weekdaycontainer.className = this.props.classnames.weekdays;
		this.elements.WeekdayNamesAndMonthdaysContainer.appendChild( this.elements.weekdaycontainer );

		this.elements.weekdays = [];
		for ( i = 0 ; i < 7; i ++ ) {
			var tmpwd = document.createElement('div');
			tmpwd.className = this.props.classnames.weekday;
			tmpwd.style.overflow = 'hidden';
			tmpwd.style.styleFloat = 'left';
			tmpwd.style.cssFloat = 'left';
			tmpwd.style['float'] = 'left';
			this.elements.weekdaycontainer.appendChild( tmpwd );
			this.elements.weekdays.push(tmpwd);
		}

		this.elements.monthdayscontainer = document.createElement('div');
		this.elements.monthdayscontainer.style.overflow = "hidden";
		this.elements.monthdayscontainer.className = this.props.classnames.monthdays;
		this.elements.WeekdayNamesAndMonthdaysContainer.appendChild( this.elements.monthdayscontainer );

		this.elements.controlscontainer = document.createElement('div');
		this.elements.controlscontainer.style.overflow = "hidden";
		this.elements.container.appendChild( this.elements.controlscontainer );

		this.elements.monthdaycells = [];

		for ( i = 0; i < 42; i ++ ) {
			var tmpcell = document.createElement('div');
			tmpcell.style.styleFloat = 'left';
			tmpcell.style.cssFloat = 'left';
			tmpcell.style['float'] = 'left';
			tmpcell.style.overflow = 'hidden';
			tmpcell.className = this.props.classnames.monthday;
			tmpcell.dataCalendarObject = this;
			tmpcell.addEventListener('click', selectDate );
			this.elements.monthdayscontainer.appendChild( tmpcell );
			this.elements.monthdaycells.push(tmpcell);

		}

		if ( typeof el != 'undefined' ) {
			el.dataCalendarObject = this;
			el.appendChild(this.elements.container);

			var attr;

			attr = el.getAttribute('data-locale');
			if ( attr !== null ) this.setLocale(attr);

			attr = el.getAttribute('data-date');
			if ( attr === null ) {
				this.setDate (
					el.getAttribute('data-day'),
					el.getAttribute('data-month') !== null ? el.getAttribute('data-month') - 1 : null,
					el.getAttribute('data-year')
				);
			} else {
				//// data-date="now"
				//// data-date="10/08/2020 00:00"
			}

			attr = el.getAttribute('data-hide-weeknumbers');
			if ( attr !== null && attr == 'true' ) this.props.showWeeknumbers = false;

			if ( el.getAttribute('onchange') !== null )
				this.addEvent('change', new Function ( "event", el.getAttribute('onchange') ) );
		}

		this.redraw();
	};

	Calendar.instances = [];

	Calendar.onload = function () {
		var calendarElements = document.getElementsByClassName(calendarClass);
		for ( var i = 0; i < calendarElements.length; i++ )
			if ( typeof calendarElements[i].dataCalendarObject == 'undefined' ) {
				
				console.log ( (new Calendar ( calendarElements[i] )) );
			}
	};

	Calendar.loadApp = function ( app ) {
		var c = new lvzwebdesktop.Calendar();
		var w = lvzwebdesktop(app.container);

		if ( w !== false ) {
			w.calendar = c;
			if ( w.type == 'window' ) {

				w.attachItem(c.elements.container)
					.setLayout('menu:minimize,close')
					.addEvent('itemattached', function ( e ) {
						e.target.calendar = e.item.dataCalendarObject;
					})
					.setWidth(c.elements.container.offsetWidth)
					.setHeight(c.elements.container.offsetHeight)
					.resizeOnContent(false)
					.disableResize()
				;
				w.props.maximizable = false;
			}
			else if ( w.type == 'widget' ) {
				w.attachItem(c.elements.container)
					.addEvent('itemattached', function ( e ) {
						e.target.calendar = e.item.dataCalendarObject;
					})
					.addEvent('properties', function ( e ) {
						var widg = e.target;
						var wind = e.propertiesWindow
						wind.calendar = widg.calendar;

						if ( ! wind.SETUP ) {

							wind.attachItem((new lvzwebdesktop.CheckboxObject())
											.setLabel('Show week numbers')
											.setValue(wind.calendar.props.showWeeknumbers)
											.addEvent('change',function(e){
												var self = e.target;
												var c = e.target.getParentOfType('WindowObject').calendar;
												c.props.showWeeknumbers=self.value;
												c.redraw();
											}))
								.attachItem((new lvzwebdesktop.FieldsetObject())
											.setLabel("Beginning of the week:")
											.attachItem((new lvzwebdesktop.RadiogroupObject())
														.addItem((new lvzwebdesktop.RadioObject()).setLabel('Monday').setValue('mon'))
														.addItem((new lvzwebdesktop.RadioObject()).setLabel('Tuesday').setValue('tue'))
														.addItem((new lvzwebdesktop.RadioObject()).setLabel('Wednesday').setValue('wed'))
														.addItem((new lvzwebdesktop.RadioObject()).setLabel('Thursday').setValue('thu'))
														.addItem((new lvzwebdesktop.RadioObject()).setLabel('Friday').setValue('fri'))
														.addItem((new lvzwebdesktop.RadioObject()).setLabel('Saturday').setValue('sat'))
														.addItem((new lvzwebdesktop.RadioObject()).setLabel('Sunday').setValue('sun'))
														.setValue(wind.calendar.props.firstday)
														.addEvent('change', function ( e ) {
															var self = e.target;
															var c = e.target.getParentOfType('WindowObject').calendar;
															c.props.firstday = self.value;
															c.redraw();
														})))
							;

							wind.SETUP = true;
						}
					})
					.setWidth(c.elements.container.offsetWidth)
					.setHeight(c.elements.container.offsetHeight)
					.setWidth(c.elements.container.offsetWidth)
					.setHeight(c.elements.container.offsetHeight)
					.deactivateControls('resize,lock')
					.activateControls('move,lock,properties,remove')
				;
			}
		}
		else {
			app.container.appendChild(c.elements.container);
		}
	};

	Calendar.__proto__.name = 'Calendar';

	Calendar.locales = {};

	Calendar.locales.english = {};
	Calendar.locales.english.daynames = {
		"mon" : "Monday",
		"tue" : "Tuesday",
		"wed" : "Wednesday",
		"thu" : "Thursday",
		"fri" : "Friday",
		"sat" : "Saturday",
		"sun" : "Sunday"
	};
	Calendar.locales.english.monthnames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

	Calendar.locales.greek = {};
	Calendar.locales.greek.daynames = {
		"mon" : "Δευτέρα",
		"tue" : "Τρίτη",
		"wed" : "Τετάρτη",
		"thu" : "Πέμπτη",
		"fri" : "Παρασκευή",
		"sat" : "Σάββατο",
		"sun" : "Κυριακή"
	};
	Calendar.locales.greek.monthnames = ["Ιανουάριος", "Φεβρουάριος", "Μάρτιος", "Απρίλιος", "Μάιος", "Ιούνιος", "Ιούλιος", "Αύγουστος", "Σεπτέμβριος", "Οκτώβριος", "Νοέμβριος", "Δεκέμβριος"];

	if (document.readyState == 'complete') Calendar.onload();

	window.addEventListener ( 'load', Calendar.onload);

	var url = new URL ( document.currentScript.src );
	var c = url.searchParams.get('$');
	if ( c !== null ) window[c].Calendar = Calendar;
	else window.Calendar = Calendar;

}) ();
