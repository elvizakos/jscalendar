# LVzJSCalendar #

Script for creating calendars and date selectors on web pages.

Demo at https://elvizakos.gitlab.io/jscalendar

## Calendar Object ##

### Calendar Object Methods ###

#### addEvent ####

#### removeEvent ####

#### fireEvent ####

#### addLocale ####

#### setLocale ####

#### setDate ####

#### showMonthList ####

#### hideMonthList ####

#### redraw ####

#### valueOf ####

### Calendar Object Properties ###

### Calendar Object Events ###

#### change ####

#### select ####

## Calendar Element ##
